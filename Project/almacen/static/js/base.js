$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

    $('#myTable thead tr').clone(true).appendTo( '#myTable thead' );
    $('#myTable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="Buscar '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    let date = new Date()

    let day = date.getDate()
    let month = date.getMonth() + 1
    let year = date.getFullYear()

    if(month < 10){
        actualdate = (`${day}-0${month}-${year}`)
      }else{
        actualdate = (`${day}-${month}-${year}`)
    }
    var table = $('#myTable').DataTable( {
        
        orderCellsTop: true,
        fixedHeader: true,
        language: leng_espanol,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: "excel",
                text: '<i class="fa fa-table" aria-hidden="true"></i> Excel',
                className: "btn btn-success",
                title: 'Reporte Los Yuyitos Excel ' + actualdate
            },
            {
                extend: "print",
                text: '<i class="fa fa-print" aria-hidden="true"></i> Imprimir ',
                className: "btn btn-primary",
                title: 'Reporte Los Yuyitos ' + actualdate
            },
            {
                extend: "pdf",
                text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF ',
                className: "btn btn-danger",
                title: 'Reporte Los Yuyitos PDF ' + actualdate
            },
        ],
    } );
});

leng_espanol = {
                sProcessing: "Procesando...",
                sLengthMenu: "Mostrar MENU registros",
                sZeroRecords: "No se encontraron resultados",
                sEmptyTable: "Ningún dato disponible en esta tabla",
                sInfo: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ ",
                sInfoEmpty: " registros del 0 al 0 de un total de 0 registros",
                sInfoFiltered: "(filtrado de un total de MAX registros)",
                sInfoPostFix: "",
                sSearch: "Buscar:",
                sUrl: "",
                sInfoThousands: ",",
                sLoadingRecords: "Cargando...",
                oPaginate: { sFirst: "Primero", sLast: "Último", sNext: "Siguiente", sPrevious: "Anterior" },
                oAria: {
                    sSortAscending: ": Activar para ordenar la columna de manera ascendente",
                    sSortDescending: ": Activar para ordenar la columna de manera descendente",
                },
            };