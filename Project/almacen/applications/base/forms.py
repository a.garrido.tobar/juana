from django import forms
from .models import Administrator, Product, Provider, Product_type, Order, Seller, PerfilUsuario, Product, Client, Debt, Sale
from django.contrib.auth.models import User


class DateInput(forms.DateInput):
    input_type = 'date'

class AdministratorForm(forms.ModelForm):
    class Meta:
        model = Administrator
        fields = [
            'rut',
            'names',
            'firstsurname',
            'lastsurname',
            'birthdate',
            'address',
            'phone_number',
        ]
        widgets = {
            'birthdate': DateInput(), 
        }

class RegistrarForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(), label='Contraseña')
    class Meta:
        model = User
        fields = ('username',
                'password',
                'email')
        labels = {
            'username': 'Nombre de usuario',        
            'password': 'Contraseña',
            'email': 'Email'
        }
        help_texts = {
            'username': '',
        }
        error_messages = {
            'username': {
                'max_length': 'Maximo 150 caracteres',
                'required': 'Requerido'
            },
            'password': {
                'required': 'Requerido'
            }
        }
    
    def __init__(self, *args, **kwargs):
        super(RegistrarForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'class': 'form-control'})
        self.fields['password'].widget.attrs.update({'class': 'form-control'})
        self.fields['email'].widget.attrs.update({'class': 'form-control'})

class RegisteredSeller(forms.ModelForm):
    class Meta:
        model = Seller
        fields = [
            'email',
            'rut',
            'names',
            'firstsurname',
            'lastsurname',
            'birthdate',
            'address',
            'phone_number',
        ]
        widgets = {
            'birthdate': DateInput()
        }
        labels = {
            'email' : 'Repite el email anterior',
            'rut': 'RUT',        
            'names': 'Nombre',
            'firstsurname': 'Apellido paterno',
            'lastsurname': 'Apellido materno',
            'birthdate': 'Fecha de naciento',
            'address': 'Dirección',
            'phone_number': 'N° de telefono',
        }
        error_messages = {
            'rut': {
                'max_length': 'Maximo 10 caracteres',
                'required': 'Requerido'
            },
            'names': {
                'max_length': 'Maximo 40 caracteres',
                'required': 'Requerido'
            },
            'firstsurname': {
                'max_length': 'Maximo 20 caracteres',
                'required': 'Requerido'
            },
            'lastsurname': {
                'max_length': 'Maximo 20 caracteres',
                'required': 'Requerido'
            },
            'address': {
                'max_length': 'Maximo 100 caracteres',
                'required': 'Requerido'
            },
            'phone_number': {
                'max_length': 'Maximo 12 caracteres',
                'required': 'Requerido'
            },
    }
    def __init__(self, *args, **kwargs):
        super(RegisteredSeller, self).__init__(*args, **kwargs)
        self.fields['rut'].widget.attrs.update({'class': 'form-control'})
        self.fields['names'].widget.attrs.update({'class': 'form-control'})
        self.fields['firstsurname'].widget.attrs.update({'class': 'form-control'})   
        self.fields['lastsurname'].widget.attrs.update({'class': 'form-control'})
        self.fields['birthdate'].widget.attrs.update({'class': 'form-control'})
        self.fields['address'].widget.attrs.update({'class': 'form-control'})
        self.fields['phone_number'].widget.attrs.update({'class': 'form-control'})
        self.fields['email'].widget.attrs.update({'class': 'form-control'})


class ProviderForm(forms.ModelForm):
    class Meta:
        model = Provider
        fields = [
            'provider_name',
            'email',
            'phone_number',
            'address',
        ]
        labels = {
            'provider_name': 'Nombre de proveedor',
            'email': 'Email',
            'phone_number': 'Numero telefónico',
            'address':'Dirección',
        }
    def __init__(self, *args, **kwargs):
        super(ProviderForm, self).__init__(*args, **kwargs)
        self.fields['provider_name'].widget.attrs.update({'class': 'form-control'})
        self.fields['email'].widget.attrs.update({'class': 'form-control'})
        self.fields['phone_number'].widget.attrs.update({'class': 'form-control'})   
        self.fields['address'].widget.attrs.update({'class': 'form-control'})


class ProductTypeForm(forms.ModelForm):
    class Meta:
        model = Product_type
        fields = [
            'typeProduct',
            'family',
        ]
        labels = {
            'typeProduct': 'Tipo de producto',
            'family': 'Familia',
        }

         
class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = [
            'shipping_date',
            'reception_date',
            'order_description',
            'total_price',
            'new_produtcs',  
            'status', 
            'provider',
            'admin',
        ]
        widgets = {
            'shipping_date': DateInput(),
            'reception_date': DateInput() 
        }
        labels = {
            'shipping_date': 'Fecha de envío',
            'reception_date': 'Fecha de recepción (estimada)',
            'order_description':'Contenido de la orden de pedido',
            'total_price': 'Precio total (estimado)',
            'new_produtcs': 'Productos nuevos',
            'status': 'Estado',
            'provider': 'Proveedor',
            'admin': 'Administrador a cargo',
        }
    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        self.fields['shipping_date'].widget.attrs.update({'class': 'form-control'})
        self.fields['reception_date'].widget.attrs.update({'class': 'form-control'})   
        self.fields['order_description'].widget.attrs.update({'class': 'form-control'})
        self.fields['total_price'].widget.attrs.update({'class': 'form-control'})
        self.fields['new_produtcs'].widget.attrs.update({'class': 'form-control'}) 
        self.fields['status'].widget.attrs.update({'class': 'form-control'})
        self.fields['provider'].widget.attrs.update({'class': 'form-control'}) 
        self.fields['admin'].widget.attrs.update({'class': 'form-control'})   


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = [
            'barcode',
            'brand',
            'description',
            'purchase_price',
            'sale_price',
            'expiry_date',  
            'stock', 
            'critical_stock',
            'product_type',
            'order'
        ]
        widgets = {
            'expiry_date': DateInput(),
        }
        labels = {
            'barcode': 'Codigo de barra',
            'brand': 'Marca',
            'description': 'Descripción',
            'purchase_price':'Precio compra',
            'sale_price': 'Precio venta',
            'expiry_date': 'Fecha de expiración',
            'stock': 'Stock',
            'critical_stock': 'Stock critico',
            'product_type': 'Tipo de producto',
            'order': 'Orden',
        }
    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['barcode'].widget.attrs.update({'class': 'form-control'})
        self.fields['brand'].widget.attrs.update({'class': 'form-control'})
        self.fields['description'].widget.attrs.update({'class': 'form-control'})   
        self.fields['purchase_price'].widget.attrs.update({'class': 'form-control'})
        self.fields['expiry_date'].widget.attrs.update({'class': 'form-control'})
        self.fields['stock'].widget.attrs.update({'class': 'form-control'}) 
        self.fields['critical_stock'].widget.attrs.update({'class': 'form-control'})
        self.fields['product_type'].widget.attrs.update({'class': 'form-control'}) 
        self.fields['order'].widget.attrs.update({'class': 'form-control'})   

class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = [
            'names',
            'firstsurname',
            'lastsurname',
            'address',
            'phone_number',
            'email',
            'debt_quota',
        ]
        labels = {
            'names': 'Nombres',
            'firstsurname': 'Apellido paterno',
            'lastsurname': 'Apellido materno',
            'address': 'Dirección',
            'phone_number': 'Numero de telefóno',
            'debt_quota': 'Debt quota',
        }
    def __init__(self, *args, **kwargs):
        super(ClientForm, self).__init__(*args, **kwargs)
        self.fields['names'].widget.attrs.update({'class': 'form-control'})
        self.fields['firstsurname'].widget.attrs.update({'class': 'form-control'})
        self.fields['lastsurname'].widget.attrs.update({'class': 'form-control'})   
        self.fields['address'].widget.attrs.update({'class': 'form-control'})
        self.fields['phone_number'].widget.attrs.update({'class': 'form-control'})
        self.fields['email'].widget.attrs.update({'class': 'form-control'}) 
        self.fields['debt_quota'].widget.attrs.update({'class': 'form-control'})

class DebtForm(forms.ModelForm):
    class Meta:
        model = Debt
        fields = [
            'amount',
            'final_date',
            'client'
        ]
        widgets = {
            'final_date': DateInput(), 
        }
        labels = {
            'amount': 'Monto',
            'final_date': 'Fecha pago',
            'client': 'Cliente',
        }
    def __init__(self, *args, **kwargs):
        super(DebtForm, self).__init__(*args, **kwargs)
        self.fields['amount'].widget.attrs.update({'class': 'form-control'})
        self.fields['final_date'].widget.attrs.update({'class': 'form-control'})
        self.fields['client'].widget.attrs.update({'class': 'form-control'})

class SaleForm(forms.ModelForm):
    class Meta:
        model = Sale
        fields = [
            'amount',
            'debt',
            'seller'
        ]
        labels = {
            'amount':'Total',
            'debt': 'Deuda',
            'seller': 'Vendedor',
        }
    def __init__(self, *args, **kwargs):
        super(SaleForm, self).__init__(*args, **kwargs) 
        self.fields['amount'].widget.attrs.update({'class': 'form-control'})
        self.fields['debt'].widget.attrs.update({'class': 'form-control'})
        self.fields['seller'].widget.attrs.update({'class': 'form-control'}) 
