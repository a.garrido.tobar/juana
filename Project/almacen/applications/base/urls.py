from django.urls import path
# from django.conf.urls import url
from .views import registrar_admin, edit_order, login_admin, index, add_order, list_order, order_reception, AgregarProducto, listar_producto, editar_producto, usuario_logout, add_client, add_debt, list_debt, add_seller, edit_client, edit_debt, add_provider, list_provider, edit_provider, mod_seller, venta_principal, edit_seller, end_sale, listar_venta, editar_venta
from django.contrib.auth import views as auth_views

app_name = 'base'

urlpatterns = [
    path('login/', login_admin, name = 'login'),
    path('registrarcuenta/', registrar_admin, name= 'registrarcuenta'),
    path('logout/', usuario_logout, name = 'logout'),
    path('add_order', add_order, name= 'add_order'),
    path('list_order', list_order, name='list_order'),
    path('edit_order/<int:order_id>', edit_order, name= 'edit'),
    path('reception', order_reception, name='reception'),
    path('agregarproducto/', AgregarProducto.as_view(), name='agregarproducto'),
    path('listarproducto/', listar_producto, name= 'listarproducto'),
    path('editar_producto/<int:product_id>', editar_producto),
    path('add_client', add_client, name='add_client'),
    path('add_debt', add_debt, name='add_debt'),
    path('list_debt', list_debt, name='list_debt'),
    path('agregar-vendedor/', add_seller, name='agregar_vendedor'),
    path('edit_client/<int:client_id>', edit_client, name= 'edit_client'),
    path('edit_debt/<int:debt_id>', edit_debt, name= 'edit_debt'),
    path('add_provider', add_provider, name= 'add_provider'),
    path('list_provider', list_provider, name= 'list_provider'),
    path('edit_provider/<int:provider_id>', edit_provider, name= 'edit_provider'),
    path('modificar-vendedor/', mod_seller, name='modificar-vendedor'),
    path('edit_seller/<int:seller_id>', edit_seller),
    path('venta_principal/', venta_principal, name='ventaprincipal'),
    path('venta_terminada/', end_sale, name='venta_terminada'),
    path('listar_venta/', listar_venta, name= 'listarventa'),
    path('editar_venta/<int:sale_id>', editar_venta),
    path('', index, name='index'),
]


