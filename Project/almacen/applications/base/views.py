import datetime
from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse, reverse_lazy
from .forms import AdministratorForm, ProductForm, RegistrarForm, ProductTypeForm, ProviderForm, OrderForm, ClientForm, DebtForm, RegisteredSeller, SaleForm
from .models import Client, Administrator, Product, Provider, Order, Debt, Seller, Product_On_Sale, total_sale, Sale, Product_On_Sale_Completed
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

def index(request):
    return render(request, 'index.html', {})

def password_reset(request):
    return print("PASS RESET")

def login_admin(request):
        # Recibe formulario mediante metodo POST
    if request.method == 'POST':
        # Recibimos la informacion del formulario
        username = request.POST.get('username')
        password = request.POST.get('password')
        # Autenticamos el usuario
        user = authenticate(username=username, password=password)
        # Verifica que exista el usuario
        if user:
            # Verifica si el usuario esta activo
            if user.is_active:
                # Genera un login para autenticar al usuario
                login(request, user)
                return HttpResponseRedirect(reverse('base:index'))
            else:
                return HttpResponse("Tu cuenta esta inactiva.")
        else:
            # Si no existe usuario
            # print("username: {} - password: {}".format(username, password))
            return HttpResponse("Datos inválidos")
    else:  # Si llega desde una url en metodo GET (desde el navegador)
        return render(request, 'login.html', {})

def usuario_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('base:index'))

def registrar_admin(request):
    registrado = False
    # Recibe formulario mediante metodo POST
    if request.method == 'POST':
        # Crea formulario de usuario con informacion del request
        user_form = RegistrarForm(data=request.POST)
        # Crea formulario de perfil con informacion del request
        profile_form = AdministratorForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            # Guardamos en base de datos
            user = user_form.save()
            # Encripta password con el modelo de django
            user.set_password(user.password)
            # Guarda usuario tras encriptacion
            user.save()
            # Instancia un objeto perfil
            profile = profile_form.save(commit=False)
            # Asigna un usario al perfil
            profile.user = user
            # Guarda en base de datos
            profile.save()
            registrado = True
        else:  # Si alguno de los formularios es invalido
            print(user_form.errors, profile_form.errors)
            return HttpResponse("Datos inválidos")
    else:  # Si no es POST generamos los formularios
        user_form = RegistrarForm()
        profile_form = AdministratorForm
    return render(request, 'administrator_form.html',
                  {'user_form': user_form,
                   'profile_form': profile_form,
                   'registrado': registrado})

def add_seller(request):
    registered = False
    if request.method == 'POST':
        # Crea formulario deL VENDEDOR con informacion del request
        seller_form = RegisteredSeller(data=request.POST)
        # Crea formulario de perfil con informacion del request
        user_form = RegistrarForm(data=request.POST)
        if seller_form.is_valid() and user_form.is_valid():
            # Guardamos en base de datos
            user = user_form.save()
            # Encripta password con el modelo de django
            user.set_password(user.password)
            # Guarda usuario tras encriptacion
            user.save()
            # Instancia un objeto perfil
            profile = seller_form.save(commit=False)
            # Asigna un usario al perfil
            profile.user = user
            # Guarda en base de datos
            profile.save()
            registered = True
        else:  # Si alguno de los formularios es invalido
            print(seller_form.errors, user_form.errors)
            return HttpResponse("Datos invalidos", registered)
    else:  # Si no es POST generamos los formularios
        seller_form = RegisteredSeller()
        user_form = RegistrarForm
    return render(request, 'maintainers/seller/add_seller.html',
                    {
                        'seller_form': seller_form,
                        'user_form': user_form,
                        'registered': registered
                    })

def mod_seller(request):
    # creamos una colección la cual carga TODOS los registos
    sellers = Seller.objects.all().order_by('-id')
    # users = User.objects.all().filter(sellers) 
    return render(request, 'maintainers/seller/mod_seller.html', {'sellers' : sellers})

def edit_seller(request, seller_id):
    # Recuperamos el registro de la base de datos por el id
    instancia = Seller.objects.get(id=seller_id)
    # creamos un formulario con los datos del objeto
    form = RegisteredSeller(instance=instancia)
    # Compronbamos si se envió el formulario
    if request.method == "POST":
        # Actualizamos el formulario con los datos del objeto
        form = RegisteredSeller(request.POST, instance=instancia)
        # Si el formulario es valido....
        if form.is_valid():
            # Guardamos el formulario pero sin confirmar aun
            instancia = form.save(commit=False)
            # grabamos!!!
            instancia.save()

            return redirect('base:modificar-vendedor')
    return render(request, "maintainers/seller/edit_seller.html", {'form': form, 'instancia': instancia})


def list_order(request):
        # creamos una colección la cual carga TODOS los registos
        order = Order.objects.all()
        # renderizamos la colección en el template
        return render(request,
            "list_order.html", {'order': order})

def order_reception(request):
        # creamos una colección la cual carga TODOS los registos
        order = Order.objects.all()
        product_form=ProductForm()
        if request.method== "POST":
            form = ProductForm(request.POST)
            if form.is_valid():
                instancia = form.save(commit=False)
                instancia.save()
                return redirect ('base:reception')
        # renderizamos la colección en el template
        return render(request,
            "order_reception.html", {'order': order, 'product_form': product_form})

def edit_order(request, order_id):
    # Recuperamos el registro de la base de datos por el id
    instancia = Order.objects.get(id=order_id)
    # creamos un formulario con los datos del objeto
    form = OrderForm(instance=instancia)
    # Compronbamos si se envió el formulario
    if request.method == "POST":
        # Actualizamos el formulario con los datos del objeto
        form = OrderForm(request.POST, instance=instancia)
        # Si el formulario es valido....
        if form.is_valid():
            # Guardamos el formulario pero sin confirmar aun
            instancia = form.save(commit=False)
            # grabamos!!!
            instancia.save()
            return redirect('base:list_order')
    return render(request, "edit_order.html", {'form': form, 'instancia': instancia})

def add_order(request):
    # Creamos un formulario vacío
    order_form = OrderForm()
    # Comprobamos si se ha enviado el formulario
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = OrderForm(request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Guardamos el formulario pero sin confirmarlo,
            # así conseguiremos una instancia para manejarla
            instancia = form.save(commit=False)
            # Podemos guardarla cuando queramos
            instancia.save()
            # Después de guardar redireccionamos a la lista
            return redirect('base:add_order')
            

    # Si llegamos al final renderizamos el formulario
    return render(request, "add_order.html", {'order_form': order_form})

def listar_producto(request):
    product = Product.objects.all()
    return render(request,
    "consultar_producto.html", {'product': product})

def editar_producto(request, product_id):
    # Recuperamos el registro de la base de datos por el id
    instancia = Product.objects.get(id=product_id)
    # creamos un formulario con los datos del objeto
    form = ProductForm(instance=instancia)
    # Compronbamos si se envió el formulario
    if request.method == "POST":
        # Actualizamos el formulario con los datos del objeto
        form = ProductForm(request.POST, instance=instancia)
        # Si el formulario es valido....
        if form.is_valid():
            # Guardamos el formulario pero sin confirmar aun
            instancia = form.save(commit=False)
            # grabamos!!!
            instancia.save()
            return redirect('base:listarproducto')
    return render(request, "modificar_producto.html", {'form': form})

class AgregarProducto(CreateView):
    model = Product
    template_name = 'agregar_producto.html'
    form_class = ProductForm
    success_url = '/agregarproducto/'


def add_client(request):
    # Creamos un formulario vacío
    client_form = ClientForm()
    # Comprobamos si se ha enviado el formulario
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = ClientForm(request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Guardamos el formulario pero sin confirmarlo,
            # así conseguiremos una instancia para manejarla
            instancia = form.save(commit=False)
            # Podemos guardarla cuando queramos
            instancia.save()
            # Después de guardar redireccionamos a la lista
            return redirect('base:add_debt')
    # Si llegamos al final renderizamos el formulario
    return render(request, "add_client.html", {'client_form': client_form})

def add_debt(request):
    # Creamos un formulario vacío
    debt_form = DebtForm()
    # Comprobamos si se ha enviado el formulario
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = DebtForm(request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Guardamos el formulario pero sin confirmarlo,
            # así conseguiremos una instancia para manejarla
            instancia = form.save(commit=False)
            # Podemos guardarla cuando queramos
            instancia.save()
            # Después de guardar redireccionamos a la lista
            return redirect('base:list_debt')
    # Si llegamos al final renderizamos el formulario
    return render(request, "add_debt.html", {'debt_form': debt_form})

def list_debt(request):
    client = Client.objects.all()
    debt = Debt.objects.all()
    mylist = zip(client, debt)
    return render(request,
    "list_debt.html", {'lista': mylist})

def edit_client(request, client_id):
    # Recuperamos el registro de la base de datos por el id
    instancia_cli = Client.objects.get(id=client_id)
    # creamos un formulario con los datos del objeto
    form_cli = ClientForm(instance=instancia_cli)
    # Compronbamos si se envió el formulario
    if request.method == "POST":
        # Actualizamos el formulario con los datos del objeto
        form_cli = ClientForm(request.POST, instance=instancia_cli)
        # Si el formulario es valido....
        if form_cli.is_valid():
            # Guardamos el formulario pero sin confirmar aun
            instancia_cli = form_cli.save(commit=False)
            # grabamos!!!
            instancia_cli.save()
            return redirect('base:list_debt')
    return render(request, "edit_client.html", {'form_cli': form_cli, 'instancia_cli': instancia_cli})

def edit_debt(request, debt_id):
    # Recuperamos el registro de la base de datos por el id
    instancia_debt = Debt.objects.get(id=debt_id)
    # creamos un formulario con los datos del objeto
    form_debt = DebtForm(instance=instancia_debt)
    # Compronbamos si se envió el formulario
    if request.method == "POST":
        # Actualizamos el formulario con los datos del objeto
        form_debt = DebtForm(request.POST, instance=instancia_debt)
        # Si el formulario es valido....
        if form_debt.is_valid():
            # Guardamos el formulario pero sin confirmar aun
            instancia_debt = form_debt.save(commit=False)
            # grabamos!!!
            instancia_debt.save()
            return redirect('base:list_debt')
    return render(request, "edit_debt.html", {'form_debt': form_debt, 'instancia_debt': instancia_debt})


def add_provider(request):
    # Creamos un formulario vacío
    provider_form = ProviderForm()
    # Comprobamos si se ha enviado el formulario
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = ProviderForm(request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Guardamos el formulario pero sin confirmarlo,
            # así conseguiremos una instancia para manejarla
            instancia = form.save(commit=False)
            # Podemos guardarla cuando queramos
            instancia.save()
            # Después de guardar redireccionamos a la lista
            return redirect('base:add_provider')
    # Si llegamos al final renderizamos el formulario
    return render(request, "add_provider.html", {'provider_form': provider_form})

def list_provider(request):
    provider = Provider.objects.all()
    return render(request,
    "list_provider.html", {'provider': provider})


def edit_provider(request, provider_id):
    # Recuperamos el registro de la base de datos por el id
    instancia = Provider.objects.get(id=provider_id)
    # creamos un formulario con los datos del objeto
    form = ProviderForm(instance=instancia)
    # Compronbamos si se envió el formulario
    if request.method == "POST":
        # Actualizamos el formulario con los datos del objeto
        form = ProviderForm(request.POST, instance=instancia)
        # Si el formulario es valido....
        if form.is_valid():
            # Guardamos el formulario pero sin confirmar aun
            instancia = form.save(commit=False)
            # grabamos!!!
            instancia.save()
            return redirect('base:list_provider')
    return render(request, "edit_provider.html", {'form': form})

def venta_principal(request):
    products = Product.objects.filter()
    products_on_sale = Product_On_Sale.objects.filter()
    new_total = total_sale.objects.last()

    print("GET::: ", request.GET)

    # Curso para cuando la peticion es GET y se esta 
    # agregando un nuevo producto a la venta temporal
    if request.method == 'GET' and request.GET.get("sale_price") != None:
        # Asignacion de variables
        barcode = request.GET.get("barcode")
        brand = request.GET.get("brand")
        description = request.GET.get("description")
        sale_price = int(request.GET.get("sale_price"))
        quantity = int(request.GET.get("cantidad"))

        # Total($) = Cantidad x Precio
        current_total = int(quantity * sale_price)
        print("CURRENT TOTAL:: ", current_total)

        # Instancia (Objeto) del modelo Product_On_sale
        # con las variables recibidas y total calculado 
        new_product_on_sale = Product_On_Sale.create(barcode, quantity, current_total)

        # Se registra el producto momentaneo
        new_product_on_sale.save()

        # si la tabla contiene registros, obtener ese total y calcular
        # Si new_total.total contiene valores, significa que la venta 
        # no a terminado y se debe capturar el total anterior
        # y sumar al current total
        if new_total:
            new_total.total = int(new_total.total + current_total)
            print("NEW TOTAL::::: ", new_total.total)

            new_total.save()

            return render(request, 
                'sale/venta_principal.html', 
                {
                    'products' : products, 
                    'products_on_sale' : products_on_sale,
                    'new_total' : new_total.total
                }
            )  
        else:
            # de no contener registros, el nuevo total es current_total
             # Total($) = Cantidad x Precio
            current_total = int(quantity * sale_price)
            print("CURRENT TOTAL22:: ", current_total)

            new_total = total_sale.create(current_total)
            new_total.save()

            # return para cuando el total es insertado por primera vez
            return render(request,
                'sale/venta_principal.html', 
                {
                    'products' : products, 
                    'products_on_sale' : products_on_sale,
                    'new_total' : new_total.total
                }
            )

    # si GET trae la variable "nuevo_total" entonces:
    if request.GET.get("nuevo_total"):    
        print("INIT END SALE :::")
        # obtener el nuevo total
        new_amount = request.GET.get("nuevo_total")
        # fiado en falso por defecto
        new_debt = False
        
        # Obtener entidad vendedor segun email
        new_seller = Seller.objects.get(email=request.user.email)
        print("SELLER::: ", new_seller)

        # Obtener fecha actual del servidor para la venta
        create = datetime.datetime.now()
        print("CREATED SALE - ::: ", create)

        # Instancia al modelo Sale
        newSale = Sale.create(create, new_amount, new_debt, new_seller)
        print("REGISTERED NEW SALE INSTANCE ::: ", newSale)
        
        # Aqui deberiamos guardar los products_on_sale 
        # definitivos
        #products_sale_completed = Product_On_Sale_Completed.create()


        # Guardar la instancia de la venta
        newSale.save()

        # Eliminar productos temporales y eliminar total tempora
        ## products_on_sale = Product_On_Sale.objects.filter()
        ## new_total = total_sale.objects.last()
        Product_On_Sale.objects.filter().delete()
        total_sale.objects.filter().delete()

        # return para cuando se finaliza la venta
        return HttpResponseRedirect(reverse('base:venta_terminada'))

    # return para cuando entra por primera vez a la vista
    return render(request, 
        'sale/venta_principal.html', 
        {
            'products' : products, 
            'products_on_sale' : products_on_sale,
            'new_total' : new_total
        }
    )

def finalizar_venta(request):
    print("REQUES::: ", request)

def end_sale(request):
    print("END SALE :::")
    last_sale = Sale.objects.filter().last()
    return render(request, 
        'sale/venta_terminada.html', 
        {
            'new_total' : last_sale.amount
        })

def listar_venta(request):
    sale = Sale.objects.all()
    return render(request,
    "consultar_venta.html", {'sale': sale})

def editar_venta(request, sale_id):
    # Recuperamos el registro de la base de datos por el id
    instancia = Sale.objects.get(id=sale_id)
    # creamos un formulario con los datos del objeto
    form = SaleForm(instance=instancia)
    # Compronbamos si se envió el formulario
    if request.method == "POST":
        # Actualizamos el formulario con los datos del objeto
        form = SaleForm(request.POST, instance=instancia)
        # Si el formulario es valido....
        if form.is_valid():
            # Guardamos el formulario pero sin confirmar aun
            instancia = form.save(commit=False)
            # grabamos!!!
            instancia.save()
    return render(request, "modificar_venta.html", {'form': form})
