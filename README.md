# JUANA PROJECT
The __"Juana project"__  is a small business application that includes sales, product, debt and user management.

This project is created using the __Django framework__. On its official site, you can find all the necessary documentation to get started.

https://www.djangoproject.com/

As database engine used __SQlite3__.

https://www.sqlite.org/index.html

## Main branches
* __main__ is the branch of production which contains the latest version of the project.
* __dev__ is the development branch, which is used by developers to integrate and test new functionalities.
* __qa__ 
is the testing branch, here the changes are uploaded to be tested and ensure quality performance.


## Previous Installation

  Python __https://www.python.org/downloads/__ 
  select the installation with AUTOMATIC route integration.

## Project installation

Clone the project from https://gitlab.com/a.garrido.tobar/juana which will download from the "main" branch the files necessary for its execution. Then you must enter the Project/almacen path from the command console and run:
* __python3 manage.py runserver__


## How to use

In order to access the application's functionalities, you must first create a superuser or use the __default user__, for this locate yourself in the project folder that contains the file "manage.py" from the command console and execute the following line:

* __python3 manage.py createsuperuser__

### Default user

* User: dev3
* Pass: dev3


If create new user, enter the requested fields, it is important to enter an email that you can access to recover your password.

#### Note 
Depending on the version that was installed, the python command may vary. 
